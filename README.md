# Virtual ATM

Withdraws and deposits money into user's accounts, generates statements.
Since money is a combination of a number and a currency, every user can have multiple accounts in different currencies.
All errors and dirty data should be handled gracefully.

Built in maven, no specific config needed.
JUnit 4 unit and integration tests (with some features lacking due to SpringRunner).
100% coverage where it makes sense.
h2 database.


Curl examples can be found in curl_example.txt


