package com.banking.virtualatm.controller;

import com.banking.virtualatm.api.datatype.BusinessException;
import com.banking.virtualatm.api.datatype.ResultStatus;
import com.banking.virtualatm.api.controller.VirtualATMController;
import com.banking.virtualatm.api.request.*;
import com.banking.virtualatm.api.response.*;
import com.banking.virtualatm.api.service.AccountService;
import com.banking.virtualatm.api.service.UserService;
import com.banking.virtualatm.entity.Account;
import com.banking.virtualatm.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.math.BigInteger;

@RestController
public class VirtualATMControllerImpl implements VirtualATMController {
    @Autowired
    @Qualifier("accountService")
    private AccountService accountService;

    @Autowired
    @Qualifier("userService")
    private UserService userService;


    @RequestMapping("/registerUser")
    @Transactional
    public RegisterUserResponse registerUser(@RequestBody @Valid RegisterUserRequest registerUserRequest) {
        if (!registerUserRequest.getPassword().equals(registerUserRequest.getConfirmPassword())) {
            throw new BusinessException(ResultStatus.ERROR, "Passwords do not match.");
        }
        userService.createUser(registerUserRequest.getEmail(), registerUserRequest.getPassword());
        return new RegisterUserResponse();
    }

    @RequestMapping("/depositFunds")
    @Transactional
    public DepositFundsResponse depositFunds(@RequestBody @Valid DepositFundsRequest req) {
        userService.login(req.getEmail(), req.getPassword());
        User user = userService.getByEmail(req.getEmail());
        Account account = accountService.getOrCreateByUserAndCurrency(user, req.getCurrency());

        accountService.makeTransaction(account, req.getAmount(), req.getDescription());
        return new DepositFundsResponse();
    }

    @RequestMapping("/withdrawFunds")
    @Transactional
    public WithdrawFundsResponse withdrawFunds(@RequestBody @Valid WithdrawFundsRequest req) {
        userService.login(req.getEmail(), req.getPassword());
        User user = userService.getByEmail(req.getEmail());
        Account account = accountService.getOrCreateByUserAndCurrency(user, req.getCurrency());

        accountService.makeTransaction(account, new BigInteger("0").subtract(req.getAmount()), req.getDescription());
        return new WithdrawFundsResponse();
    }

    @RequestMapping("/getStatement")
    @Transactional(readOnly = true)
    public GetStatementResponse getStatement(@RequestBody @Valid GetStatementRequest req) {
        userService.login(req.getEmail(), req.getPassword());
        User user = userService.getByEmail(req.getEmail());
        Account account = accountService.getOrCreateByUserAndCurrency(user, req.getCurrency());

        return new GetStatementResponse(account.getBalance(), accountService.getStatement(account));
    }
}
