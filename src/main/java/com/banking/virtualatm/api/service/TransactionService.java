package com.banking.virtualatm.api.service;

import com.banking.virtualatm.entity.Account;
import com.banking.virtualatm.entity.Transaction;

import java.math.BigInteger;

public interface TransactionService {

    Transaction createTransaction(Account account, BigInteger amount, String description);

    Iterable<Transaction> findByAccountOrderByTimestampDesc(Account account);
}
