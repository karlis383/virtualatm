package com.banking.virtualatm.api.service;

import com.banking.virtualatm.entity.User;

public interface UserService {
    User getByEmail(String email);

    User createUser(String email, String password);

    User getOrCreateUser(String email, String password);

    void login(String email, String password);
}
