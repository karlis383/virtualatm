package com.banking.virtualatm.api.service;

import com.banking.virtualatm.api.datatype.Currency;
import com.banking.virtualatm.api.dto.StatementRow;
import com.banking.virtualatm.entity.Account;
import com.banking.virtualatm.entity.User;

import java.math.BigInteger;
import java.util.List;

public interface AccountService {
    Account getOrCreateByUserAndCurrency(User user, Currency currency);

    void makeTransaction(Account account, BigInteger amount, String description);

    List<StatementRow> getStatement(Account account);
}
