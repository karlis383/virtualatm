package com.banking.virtualatm.api.datatype;

import org.springframework.http.HttpStatus;

public enum ResultStatus {
    SUCCESS(HttpStatus.OK, "Success"),
    ERROR(HttpStatus.BAD_REQUEST, "{0}"),
    NOT_FOUND(HttpStatus.NOT_FOUND, "{0} not found"),
    FORBIDDEN(HttpStatus.FORBIDDEN, "Forbidden"),
    UNAUTHORIZED(HttpStatus.UNAUTHORIZED, "Unauthorized"),
    systemError(HttpStatus.INTERNAL_SERVER_ERROR, "System Error");

    private final HttpStatus status;
    private final String message;

    ResultStatus(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}

