package com.banking.virtualatm.api.datatype;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Currency {
    EUR("EUR"),
    USD("USD"),
    AUD("AUD");

    private final String value;

    Currency(final String value) {
        this.value = value;
    }

    @JsonCreator
    public static Currency fromString(String value) {
        if (value == null) {
            throw new BusinessException(ResultStatus.ERROR, "Currency must not be empty.");
        }
        try {
            return Currency.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException ex) {
            throw new BusinessException(ResultStatus.ERROR, "Invalid currency.");
        }
    }

    @JsonValue
    public String toValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
