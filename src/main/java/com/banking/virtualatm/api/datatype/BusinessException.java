package com.banking.virtualatm.api.datatype;

import java.text.MessageFormat;

public class BusinessException extends RuntimeException {

    private final ResultStatus result;

    public BusinessException(ResultStatus result, Object... messageArguments) {
        super(MessageFormat.format(result.getMessage(), messageArguments));
        this.result = result;
    }

    public ResultStatus getResult() {
        return result;
    }

    @Override
    public synchronized Throwable fillInStackTrace() { //Business exception needs no stack trace. This speeds up exception creation very considerably.
        return this;
    }
}
