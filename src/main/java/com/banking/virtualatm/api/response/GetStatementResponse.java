package com.banking.virtualatm.api.response;

import com.banking.virtualatm.api.datatype.ResultStatus;
import com.banking.virtualatm.api.dto.StatementRow;

import java.math.BigInteger;
import java.util.List;

public class GetStatementResponse extends BaseResponse {
    private List<StatementRow> transactions;

    private BigInteger balance;

    public GetStatementResponse() {
        super(ResultStatus.systemError, ResultStatus.systemError.getMessage()); //this should never happen
    }

    public GetStatementResponse(BigInteger balance, List<StatementRow> transactions) {
        super();
        this.balance = balance;
        this.transactions = transactions;
    }

    public List<StatementRow> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<StatementRow> transactions) {
        this.transactions = transactions;
    }

    public BigInteger getBalance() {
        return balance;
    }

    public void setBalance(BigInteger balance) {
        this.balance = balance;
    }
}
