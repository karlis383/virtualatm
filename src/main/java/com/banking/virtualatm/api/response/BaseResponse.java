package com.banking.virtualatm.api.response;

import com.banking.virtualatm.api.datatype.ResultStatus;
import org.springframework.http.HttpStatus;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class BaseResponse implements Serializable {

    @NotNull
    private HttpStatus status;

    private String message;

    BaseResponse() {
        this.status = HttpStatus.OK;
        this.message = "Success";
    }

    public BaseResponse(@NotNull ResultStatus result, String message) {
        this.status = result.getStatus();
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
