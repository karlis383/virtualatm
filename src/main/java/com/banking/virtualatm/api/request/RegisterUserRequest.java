package com.banking.virtualatm.api.request;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class RegisterUserRequest {

    @Email
    @NotNull
    private String email;

    @NotNull
    @Length(min = 6, max = 64)
    private String password;

    @NotNull
    @Length(min = 6, max = 64)
    private String confirmPassword;

    public RegisterUserRequest() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
