package com.banking.virtualatm.api.request;

import com.banking.virtualatm.api.datatype.Currency;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class GetStatementRequest {
    @Email
    @NotNull
    private String email;

    @NotNull
    @Length(min = 6, max = 64)
    private String password;

    private Currency currency;

    public GetStatementRequest() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
