package com.banking.virtualatm.api.dto;

import java.math.BigInteger;
import java.util.Date;

public class StatementRow {
    private Date timestamp;

    private String description;

    private BigInteger change;

    private BigInteger balance;

    public StatementRow() {
    }

    public StatementRow(Date timestamp, String description, BigInteger change, BigInteger balance) {
        this.timestamp = timestamp;
        this.description = description;
        this.change = change;
        this.balance = balance;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigInteger getChange() {
        return change;
    }

    public void setChange(BigInteger change) {
        this.change = change;
    }

    public BigInteger getBalance() {
        return balance;
    }

    public void setBalance(BigInteger balance) {
        this.balance = balance;
    }
}
