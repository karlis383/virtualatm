package com.banking.virtualatm.api.controller;

import com.banking.virtualatm.api.request.*;
import com.banking.virtualatm.api.response.*;

import javax.validation.Valid;

public interface VirtualATMController {

    RegisterUserResponse registerUser(@Valid RegisterUserRequest req);

    DepositFundsResponse depositFunds(@Valid DepositFundsRequest req);

    WithdrawFundsResponse withdrawFunds(@Valid WithdrawFundsRequest req);

    GetStatementResponse getStatement(@Valid GetStatementRequest req);
}
