package com.banking.virtualatm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
class VirtualATMApplication {

    public static void main(String[] args) {
        SpringApplication.run(VirtualATMApplication.class, args);
    }

}

