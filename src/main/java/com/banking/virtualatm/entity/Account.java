package com.banking.virtualatm.entity;

import com.banking.virtualatm.api.datatype.Currency;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;

@Entity
public class Account {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private User user;

    @Column(nullable = false)
    private Currency currency;

    @Column(nullable = false)
    private BigInteger balance;

    @OneToMany(mappedBy = "account")
    private List<Transaction> transactions;

    public Account(User user, Currency currency) {
        this.user = user;
        this.currency = currency;
        this.balance = new BigInteger("0");
    }

    public Account() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigInteger getBalance() {
        return balance;
    }

    public void setBalance(BigInteger balance) {
        this.balance = balance;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
