package com.banking.virtualatm.entity;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Entity
public class Transaction {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Account account;

    @Column(nullable = false)
    private Date timestamp;

    @Column(nullable = false)
    private BigInteger amount;

    private String description;

    public Transaction() {
    }

    public Transaction(Account account, BigInteger amount, String description) {
        this.account = account;
        this.amount = amount;
        this.timestamp = new Date();
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public void setAmount(BigInteger amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
