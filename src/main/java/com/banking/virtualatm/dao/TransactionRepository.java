package com.banking.virtualatm.dao;

import com.banking.virtualatm.entity.Account;
import com.banking.virtualatm.entity.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    Iterable<Transaction> findByAccountOrderByTimestampDesc(Account account);
}
