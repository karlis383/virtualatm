package com.banking.virtualatm.dao;

import com.banking.virtualatm.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

public interface UserRepository extends CrudRepository<User, Long> {

    Iterable<User> findByEmail(String email);
}
