package com.banking.virtualatm.dao;

import com.banking.virtualatm.api.datatype.Currency;
import com.banking.virtualatm.entity.Account;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface AccountRepository extends CrudRepository<Account, Long> {

    @Query("select a from Account a join a.user u where u.email=:email and a.currency=:currency")
    Iterable<Account> findByUserAndCurrency(@Param("email") String email, @Param("currency") Currency currency);
}
