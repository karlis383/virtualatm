package com.banking.virtualatm.service;

import com.banking.virtualatm.api.datatype.BusinessException;
import com.banking.virtualatm.api.datatype.ResultStatus;
import com.banking.virtualatm.api.service.UserService;
import com.banking.virtualatm.dao.UserRepository;
import com.banking.virtualatm.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository users;

    public User getByEmail(String email) {
        Iterable<User> result = users.findByEmail(email);
        if (result.iterator().hasNext()) {
            return result.iterator().next();
        }
        return null;
    }

    public User createUser(String email, String password) {
        if (email == null || password == null || email.isEmpty() || password.isEmpty()) {
            throw new RuntimeException("Invalid parameters found.");
        }
        User user = this.getByEmail(email);
        if (user != null) {
            throw new BusinessException(ResultStatus.ERROR, "User with this email already found.");
        }
        return users.save(new User(email, password));
    }

    public User getOrCreateUser(String email, String password) { //used in tests
        if (email == null || password == null || email.isEmpty() || password.isEmpty()) {
            throw new RuntimeException("Invalid parameters found.");
        }
        User user = this.getByEmail(email);
        if (user != null) {
            return user;
        }
        return users.save(new User(email, password));
    }

    public void login(String email, String password) {
        if (email == null || password == null || email.isEmpty() || password.isEmpty()) {
            throw new RuntimeException("Invalid parameters found.");
        }
        User user = this.getByEmail(email);
        if (this.getByEmail(email) == null) {
            throw new BusinessException(ResultStatus.NOT_FOUND, "User");
        }
        if (!user.getPassword().equals(password)) {
            throw new BusinessException(ResultStatus.UNAUTHORIZED);
        }
    }
}
