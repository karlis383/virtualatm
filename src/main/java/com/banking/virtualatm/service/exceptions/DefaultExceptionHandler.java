package com.banking.virtualatm.service.exceptions;

import com.banking.virtualatm.api.datatype.BusinessException;
import com.banking.virtualatm.api.datatype.ResultStatus;
import com.banking.virtualatm.api.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice("com.banking.virtualatm")
class DefaultExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    public DefaultExceptionHandler() {
    }

    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    public ResponseEntity<Object> handleBusinessException(BusinessException ex, WebRequest request) {
        return handleExceptionInternal(ex, new BaseResponse(ex.getResult(), ex.getMessage()), null, ex.getResult().getStatus(), request);
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public ResponseEntity<Object> handleException(RuntimeException ex, WebRequest request) {
        BaseResponse response = new BaseResponse(ResultStatus.systemError, ResultStatus.systemError.getMessage());
        return handleExceptionInternal(ex, response, null, HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}
