package com.banking.virtualatm.service;

import com.banking.virtualatm.api.datatype.BusinessException;
import com.banking.virtualatm.api.datatype.Currency;
import com.banking.virtualatm.api.datatype.ResultStatus;
import com.banking.virtualatm.api.dto.StatementRow;
import com.banking.virtualatm.api.service.AccountService;
import com.banking.virtualatm.api.service.TransactionService;
import com.banking.virtualatm.dao.AccountRepository;
import com.banking.virtualatm.entity.Account;
import com.banking.virtualatm.entity.Transaction;
import com.banking.virtualatm.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Component("accountService")
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountRepository accounts;

    @Autowired
    @Qualifier("transService")
    private TransactionService transactionService;

    public Account getOrCreateByUserAndCurrency(User user, Currency currency) {
        if (user == null || currency == null) {
            throw new RuntimeException("Invalid parameters found.");
        }
        Iterable<Account> result = accounts.findByUserAndCurrency(user.getEmail(), currency);
        if (result.iterator().hasNext()) {
            return result.iterator().next();
        }
        Account account = new Account(user, currency);
        return accounts.save(account);
    }

    public void makeTransaction(Account account, BigInteger amount, String description) {
        if (account == null || amount == null) {
            throw new RuntimeException("Invalid parameters found.");
        }
        if (account.getBalance().add(amount).compareTo(new BigInteger("0")) == -1) {
            throw new BusinessException(ResultStatus.ERROR, "Insufficient funds.");
        }
        transactionService.createTransaction(account, amount, description);
        account.setBalance(account.getBalance().add(amount));
        accounts.save(account);
    }

    public List<StatementRow> getStatement(Account account) {
        if (account == null) {
            throw new RuntimeException("Invalid parameters found.");
        }
        List<StatementRow> rows = new ArrayList<>();
        BigInteger balance = account.getBalance();
        for (Transaction trx : transactionService.findByAccountOrderByTimestampDesc(account)) {
            rows.add(new StatementRow(trx.getTimestamp(), trx.getDescription(), trx.getAmount(), balance));
            balance = balance.subtract(trx.getAmount());
        }
        return rows;
    }


}
