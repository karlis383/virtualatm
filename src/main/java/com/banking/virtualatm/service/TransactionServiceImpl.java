package com.banking.virtualatm.service;

import com.banking.virtualatm.api.service.TransactionService;
import com.banking.virtualatm.dao.TransactionRepository;
import com.banking.virtualatm.entity.Account;
import com.banking.virtualatm.entity.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;

@Component("transService")
class TransactionServiceImpl implements TransactionService {
    @Autowired
    private TransactionRepository transactions;

    public Transaction createTransaction(Account account, BigInteger amount, String description) {
        if (account == null || amount == null) {
            throw new RuntimeException("Invalid parameters found.");
        }
        return transactions.save(new Transaction(account, amount, description));
    }

    public Iterable<Transaction> findByAccountOrderByTimestampDesc(Account account) {
        if (account == null) {
            throw new RuntimeException("Invalid parameters found.");
        }
        return transactions.findByAccountOrderByTimestampDesc(account);
    }
}
