package com.banking.virtualatm.integration;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class VirtualATMControllerTests {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testStage1_registerFail() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post("/registerUser")
                .content("{\"email\":\"karlis@google.com\", \"password\":\"123456\", \"confirmPassword\":\"1bfsfb5\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().string(equalTo("{\"status\":\"BAD_REQUEST\",\"message\":\"Passwords do not match.\"}")));
    }

    @Test
    public void testStage2_register() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post("/registerUser")
                .content("{\"email\":\"karlis@google.com\", \"password\":\"123456\", \"confirmPassword\":\"123456\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("{\"status\":\"OK\",\"message\":\"Success\"}")));
    }

    @Test
    public void testStage3_loginFail() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post("/depositFunds")
                .content("{\"email\":\"karlis@google.com\", \"password\":\"aaaaaa\", \"currency\":\"EUR\", \"amount\":\"112\", \"description\":\"text\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.UNAUTHORIZED.value()))
                .andExpect(content().string(equalTo("{\"status\":\"UNAUTHORIZED\",\"message\":\"Unauthorized\"}")));
    }

    @Test
    public void testStage4_withdrawFail() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post("/withdrawFunds")
                .content("{\"email\":\"karlis@google.com\", \"password\":\"123456\", \"currency\":\"EUR\", \"amount\":\"112\", \"description\":\"text\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().string(equalTo("{\"status\":\"BAD_REQUEST\",\"message\":\"Insufficient funds.\"}")));
    }

    @Test
    public void testStage5_deposit() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post("/depositFunds")
                .content("{\"email\":\"karlis@google.com\", \"password\":\"123456\", \"currency\":\"EUR\", \"amount\":\"112\", \"description\":\"text\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("{\"status\":\"OK\",\"message\":\"Success\"}")));
    }

    @Test
    public void testStage6_statement1() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .post("/getStatement")
                .content("{\"email\":\"karlis@google.com\", \"password\":\"123456\", \"currency\":\"EUR\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        String stringResult = result.getResponse().getContentAsString()
                .replaceAll("\"timestamp\":\"[^\"]+\",", "");
        assertEquals("The result did not match!", stringResult, "{\"status\":\"OK\",\"message\":\"Success\",\"transactions\":[{\"description\":\"text\",\"change\":112,\"balance\":112}],\"balance\":112}");
    }

    @Test
    public void testStage7_withdraw() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post("/withdrawFunds")
                .content("{\"email\":\"karlis@google.com\", \"password\":\"123456\", \"currency\":\"EUR\", \"amount\":\"112\", \"description\":\"text\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("{\"status\":\"OK\",\"message\":\"Success\"}")));
    }

    @Test
    public void testStage8_statement2() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .post("/getStatement")
                .content("{\"email\":\"karlis@google.com\", \"password\":\"123456\", \"currency\":\"EUR\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        String stringResult = result.getResponse().getContentAsString()
                .replaceAll("\"timestamp\":\"[^\"]+\",", "");
        assertEquals("The result did not match!", stringResult, "{\"status\":\"OK\",\"message\":\"Success\",\"transactions\":[{\"description\":\"text\",\"change\":-112,\"balance\":0},{\"description\":\"text\",\"change\":112,\"balance\":112}],\"balance\":0}");
    }

    @Test
    public void testStage9_totalFail() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post("/withdrawFunds")
                .content("{")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }
}
