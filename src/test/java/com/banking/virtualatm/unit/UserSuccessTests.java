package com.banking.virtualatm.unit;

import com.banking.virtualatm.api.service.AccountService;
import com.banking.virtualatm.api.service.UserService;
import com.banking.virtualatm.entity.User;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserSuccessTests {
    @Autowired
    @Qualifier("userService")
    private UserService userService;

    @Test
    public void testStage1_testCanCreateValidUser() {
        User user = userService.createUser("abc@cd.de", "321312");
        assertNotNull("Can't create user", user);
        assertNotNull("User not inserted", user.getId());
        assertNotNull("User has no email", user.getEmail());
        assertNotNull("User has no password", user.getPassword());
    }

    @Test
    public void testStage1_testCanGetSameUser() {
        User user = userService.createUser("abc2@cd.de", "321312");
        assertEquals("Can find same user", userService.getByEmail("abc2@cd.de").getId(), user.getId());
    }

    @Test
    public void testStage2_testLoginWorks() {
        userService.login("abc@cd.de", "321312"); //would throw exception on fail
    }
}
