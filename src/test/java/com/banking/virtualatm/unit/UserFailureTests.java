package com.banking.virtualatm.unit;

import com.banking.virtualatm.api.datatype.BusinessException;
import com.banking.virtualatm.api.service.UserService;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserFailureTests {

    @Autowired
    @Qualifier("userService")
    private UserService userService;

    @Test
    public void testStage1_cantCreateUserTwice() {
        try {
            userService.createUser("cba@cd.de", "absdds");
            userService.createUser("cba@cd.de", "asdase");
            fail("User with this email was saved twice.");
        } catch (BusinessException ex) {
            assertEquals("Other exception thrown.", ex.getResult().getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    public void testStage1_cantCreateWithoutPassword() {
        try {
            userService.createUser("cba2@cd.de", null);
            fail("Create user with no password successful.");
        } catch (RuntimeException ex) {
            assertEquals("Other exception thrown.", ex.getClass(), RuntimeException.class);
            //test successful
        }
    }

    @Test
    public void testStage2_refuseIncorrectPassword() {
        try {
            userService.login("cba@cd.de", "6547467");
            fail("Login with incorrect password successful");
        } catch (BusinessException ex) {
            assertEquals("Other exception thrown.", ex.getResult().getStatus(), HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    public void testStage2_refuseIncorrectEmail() {
        try {
            userService.login("cba444@cd.de", "6547467");
            fail("Login with no email successful");
        } catch (BusinessException ex) {
            assertEquals("Other exception thrown.", ex.getResult().getStatus(), HttpStatus.NOT_FOUND);
        }
    }
}
