package com.banking.virtualatm.unit;

import com.banking.virtualatm.api.datatype.BusinessException;
import com.banking.virtualatm.api.datatype.Currency;
import com.banking.virtualatm.api.service.AccountService;
import com.banking.virtualatm.api.service.UserService;
import com.banking.virtualatm.entity.Account;
import com.banking.virtualatm.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

import javax.annotation.Resource;
import java.math.BigInteger;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountFailureTests {
    @Autowired
    @Qualifier("accountService")
    private AccountService accountService;

    @Autowired
    @Qualifier("userService")
    private UserService userService;

    private Account account;

    @Before
    public void setUp() {
        User user = userService.getOrCreateUser("ghj2@abc.com", "123456");
        account = accountService.getOrCreateByUserAndCurrency(user, Currency.EUR);
    }

    @Test
    public void testCantWithdrawNegativeFunds() {
        try {
            accountService.makeTransaction(account, new BigInteger("-1"), "Test transaction");
            fail("Withdraw with negative funds successful.");
        } catch (BusinessException ex) {
            assertEquals("Other exception thrown.", ex.getResult().getStatus(), HttpStatus.BAD_REQUEST);
        }
    }
}
