package com.banking.virtualatm.unit;

import com.banking.virtualatm.api.datatype.Currency;
import com.banking.virtualatm.api.dto.StatementRow;
import com.banking.virtualatm.api.service.AccountService;
import com.banking.virtualatm.api.service.UserService;
import com.banking.virtualatm.entity.Account;
import com.banking.virtualatm.entity.User;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountSuccessTests {
    @Autowired
    @Qualifier("accountService")
    private AccountService accountService;

    @Autowired
    @Qualifier("userService")
    private UserService userService;

    private Account account;

    private User user;

    @Before
    public void setUp() {
        user = userService.getOrCreateUser("ghj@abc.com", "123456");
        account = accountService.getOrCreateByUserAndCurrency(user, Currency.EUR);
    }

    @Test
    public void testStage1_canCreateValidAccount() {
        assertNotNull("Can't create account", account);
        assertNotNull("Account not inserted", account.getId());
        assertNotNull("Account has no user", account.getUser());
        assertNotNull("Account has no currency", account.getCurrency());
    }

    @Test
    public void testStage1_canCreateMultipleAccounts() {
        Account account2 = accountService.getOrCreateByUserAndCurrency(user, Currency.USD);
        assertNotEquals("Can't create accounts with different currencies.", account.getId(), account2.getId());
    }

    @Test
    public void testStage1_canDeposit() {
        accountService.makeTransaction(account, new BigInteger("100"), "Test transaction");
        assertEquals("Deposit did not work.", account.getBalance(), new BigInteger("100"));
    }

    @Test
    public void testStage2_canWithdraw() {
        accountService.makeTransaction(account, new BigInteger("-90"), "Test transaction");
        assertEquals("Withdraw did not work.", account.getBalance(), new BigInteger("10"));
    }

    @Test
    public void testStage3_correctStatement() {
        accountService.makeTransaction(account, new BigInteger("110"), "Test transaction");
        accountService.makeTransaction(account, new BigInteger("10"), "Test transaction");
        List<StatementRow> rows = accountService.getStatement(account);
        BigInteger sum = account.getBalance();
        Date date = new Date(Long.MAX_VALUE);
        for (StatementRow row : rows) {
            assertEquals("Incorrect balance calculation", row.getBalance(), sum);
            sum = sum.subtract(row.getChange());
            assertTrue("Statement chronology compromised", row.getTimestamp().compareTo(date) <= 0);
            date = row.getTimestamp();
            assertTrue("Negative sum found", row.getBalance().compareTo(new BigInteger("0")) > -1);
        }
        assertEquals("Missing transactions found", sum, new BigInteger("0"));
    }
}
